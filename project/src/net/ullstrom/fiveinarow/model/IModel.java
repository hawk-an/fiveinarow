package net.ullstrom.fiveinarow.model;

import java.beans.PropertyChangeListener;

public interface IModel {
    public void newGame();
    public int getValue(int row, int col);
    public boolean setValue(int row, int col, int player);
    public void addPropertyChangeListener(PropertyChangeListener newListener);
}
