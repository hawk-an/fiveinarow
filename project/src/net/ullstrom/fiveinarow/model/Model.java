package net.ullstrom.fiveinarow.model;

import net.ullstrom.fiveinarow.Constants;
import java.util.List;
import java.util.ArrayList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class Model implements IModel
{
    private int[][] board;
    private List<PropertyChangeListener> listeners;

    public Model()
    {
        board = new int[Constants.BOARD_WIDTH][Constants.BOARD_HEIGHT];
        
        // Android 4.0 does not support diamond types. :(
        listeners = new ArrayList<PropertyChangeListener>();
    }

    public Model(IModel model)
    {
        board = new int[Constants.BOARD_WIDTH][Constants.BOARD_HEIGHT];
        for(int i = 0; i < Constants.BOARD_WIDTH; i++)
        {
            for(int j = 0; j < Constants.BOARD_HEIGHT; j++)
            {
                board[i][j] = model.getValue(i, j);
            }
        }

        listeners = new ArrayList<PropertyChangeListener>();
    }

    // Instance methods
    private boolean isLegal(int row, int col)
    {
        return row < Constants.BOARD_HEIGHT && row >= Constants.BOARD_START
            && col < Constants.BOARD_WIDTH && col >= Constants.BOARD_START
            && board[row][col] == Constants.CELL_EMPTY;
    }

    private void firePropertyChangeEvent(PropertyChangeEvent e)
    {
        for(PropertyChangeListener listener : listeners)
        {
            listener.propertyChange(e);
        }
    }

    // Interface methods
    @Override
    public void newGame()
    {
        board = new int[Constants.BOARD_WIDTH][Constants.BOARD_HEIGHT];

        firePropertyChangeEvent(
            new PropertyChangeEvent(
                null,
                Constants.PROPERTY_NEW_GAME,
                null,
                null
            )
        );
    }

    @Override
    public int getValue(int row, int col)
    {
        return board[row][col];
    }

    @Override
    public boolean setValue(int row, int col, int player)
    {
        if(isLegal(row, col))
        {
            board[row][col] = player;

            Move move = new Move(row, col, player);
            
            firePropertyChangeEvent(
                new PropertyChangeEvent(
                    this,
                    Constants.PROPERTY_NEW_VALUE,
                    null,
                    move
                )
            );

            return true;
        }

        return false;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener newListener)
    {
        listeners.add(newListener);
    }
}
