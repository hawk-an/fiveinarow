package net.ullstrom.fiveinarow.model;

import java.io.Serializable;

public class Move implements Serializable
{
    private final int x;
    private final int y;
    private final int player;
    
    public Move(int x, int y, int player)
    {
        this.x = x;
        this.y = y;
        this.player = player;
    }

    public Move(Move m)
    {
        x = m.x;
        y = m.y;
        player = m.player;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public int getPlayer()
    {
        return player;
    }
}
