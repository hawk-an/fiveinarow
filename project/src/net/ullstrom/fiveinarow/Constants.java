package net.ullstrom.fiveinarow;

public class Constants {
    public static int BOARD_START = 0;
    public static int BOARD_WIDTH = 20;
    public static int BOARD_HEIGHT = 20;

    public static int CELL_EMPTY = 0;
    public static int CELL_PLAYER_CROSS = 1;
    public static int CELL_PLAYER_CIRCLE = 2;

    public static String PROPERTY_NEW_GAME = "new game";
    public static String PROPERTY_NEW_VALUE = "new value";
}
