package net.ullstrom.fiveinarow;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;


import net.ullstrom.fiveinarow.model.Model;
import net.ullstrom.fiveinarow.delegate.BoardView;
import net.ullstrom.fiveinarow.delegate.GameController;

public class Main extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		
		Model model = new Model();
		GameController contr = new GameController(model);
		BoardView view = new BoardView(this, contr);

		model.addPropertyChangeListener(view);
		
		setContentView(view);

//        setContentView(R.layout.main);
    }
}
