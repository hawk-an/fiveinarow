package net.ullstrom.fiveinarow.delegate;

import java.beans.PropertyChangeListener; 
import java.beans.PropertyChangeEvent; 
import android.content.Context;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.util.Log;

import static java.lang.Math.*;

import net.ullstrom.fiveinarow.Constants;
import net.ullstrom.fiveinarow.model.IModel;
import net.ullstrom.fiveinarow.model.Move;

public class BoardView extends View implements PropertyChangeListener 
{
	private final Context c;
    private int[][] board;
	private float[] translation = {0.0f,0.0f};
	private float[] vector = {0.0f,0.0f};
	private int[] lastplaced;
	private int scale;
	private int padding;
	private int move;
	private final GameController gamec;

	public BoardView( Context c, GameController gc)
	{
		super(c);
        board = new int[Constants.BOARD_WIDTH][Constants.BOARD_HEIGHT];
		this.c = c;
		this.gamec = gc;
		scale = 30;
		padding = 5;
		move = 0;

		this.setOnTouchListener(new GestureListener(c));
		redraw();
	}

	public void setTranslation(float dx, float dy)
	{
		vector[0] += dx/scale;
		vector[1] += dy/scale;
		redraw();
	}
	
	private void click(float screenX, float screenY)
	{
		int x = (int) floor((screenX/scale) - translation[0]);
		int y = (int) floor((screenY/scale) - translation[1]);
		
		gamec.play(x,y);
		
	}

	@Override
	public void propertyChange(PropertyChangeEvent event)
	{
		if(event.getPropertyName().compareTo(Constants.PROPERTY_NEW_VALUE) == 0)
		{
			Move m = (Move)event.getNewValue();	
			int x = m.getX();
			int y = m.getY();

			board[x][y] = m.getPlayer();
			lastplaced = new int[]{x,y};
			move++;

		}
		redraw();
	}

	private void redraw()
	{
		this.invalidate();

	}

	private void paintSquare(Canvas c, int x, int y, int player, Paint border)
	{
		//transformation
		float screenX = (translation[0] + (float)x) * scale;
		float screenY = (translation[1] + (float)y) * scale;

		Paint linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		linePaint.setColor(Color.BLACK);
		linePaint.setStrokeWidth(scale/10);
		linePaint.setStyle(Paint.Style.STROKE);
		
		if(border != null)
			c.drawRect(screenX, screenY, screenX+scale, screenY+scale, border);
		else
			c.drawRect(screenX, screenY, screenX+scale, screenY+scale, linePaint);

		if(player == Constants.CELL_PLAYER_CROSS)
		{
			c.drawLine(screenX+padding, screenY+padding,
				screenX+scale-padding, screenY+scale-padding, linePaint);
			c.drawLine(screenX+padding, screenY+scale-padding,
				screenX+scale-padding, screenY+padding, linePaint);
		}
		else if(player == Constants.CELL_PLAYER_CIRCLE)
		{
			c.drawCircle(screenX+scale/2, screenY+scale/2, scale/2 -padding, linePaint);
		}
	}

	@Override
	protected void onDraw(Canvas c)
	{

		vector[0] /= 2;
		vector[1] /= 2;

		translation[0] += vector[0];
		translation[1] += vector[1];
		

		Paint redBorder = new Paint(Paint.ANTI_ALIAS_FLAG);
		redBorder.setColor(Color.RED);
		redBorder.setStrokeWidth(scale/10);
		redBorder.setStyle(Paint.Style.STROKE);

		c.drawColor(Color.WHITE);
		for(int x = 0; x < Constants.BOARD_WIDTH; x++)
		{
			for(int y = 0; y < Constants.BOARD_HEIGHT; y++)
			{
				paintSquare(c, x, y, board[x][y], null);
			}
		}
		
		//not first move && lastplaced? then red
		if(move > 0)
			paintSquare(c, lastplaced[0], lastplaced[1], Constants.CELL_EMPTY, redBorder);

		postInvalidate();
	}

	private class GestureListener extends GestureDetector.SimpleOnGestureListener 
			implements OnTouchListener
	{
		private static final String DEBUG_TAG = "Gesture";
		private final GestureDetector gd;
	
		public GestureListener(Context c)
		{
			gd = new GestureDetector(c, this);
		}
	
		@Override
		public boolean onDown(MotionEvent event)
		{ 
			Log.d(DEBUG_TAG,"onDown: " + event.toString()); 
			return true;
		}
	
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
			Log.d(DEBUG_TAG,"onScroll: " + e1.toString() + " " + e2.toString());
			setTranslation(-distanceX, -distanceY);
			return true;
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e)
		{
			Log.d(DEBUG_TAG, "onSingleTapUp: " + e.toString());
			click(e.getX(),e.getY());
			redraw();
			return true;
		}
	
	
		@Override
		public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
		{
			Log.d(DEBUG_TAG, "onFling: " + event1.toString()+event2.toString());
			return true;
		}
	
		@Override
		public boolean onTouch(View v, MotionEvent e)
		{
			return gd.onTouchEvent(e);
		}
	
	}
	
}
