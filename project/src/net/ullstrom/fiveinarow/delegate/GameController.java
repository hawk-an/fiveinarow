package net.ullstrom.fiveinarow.delegate;

import net.ullstrom.fiveinarow.Constants;
import net.ullstrom.fiveinarow.model.IModel;
import java.util.List;
import java.util.ArrayList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class GameController
{
	private final IModel model;
	private int player;

	public GameController(IModel m)
	{
		this.model = m;
		player = 1;
	}


	public void play(int x, int y)
	{
		if(model.setValue(x,y,player))
			player = opponent(player); 
	}

	public static int opponent(int player)
	{
		if(player == 1)
			player = 2;
		else
			player = 1;
		return player;
	}
}
